import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StructureConfigurationComponent } from './structure-configuration.component';
import { FacultyComponent } from './faculty/faculty.component';
import { ListFacultyComponent } from './faculty/list-faculty/list-faculty.component';

const routes: Routes = [
  {
    path: '',
    component: StructureConfigurationComponent,
    children: [
      {
        path: 'overview',
        component: FacultyComponent,
      },
      { path: '', redirectTo: 'overview', pathMatch: 'full' },
      { path: '**', redirectTo: 'overview', pathMatch: 'full' },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StructureConfigurationRoutingModule {}