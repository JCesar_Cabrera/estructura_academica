import { Component, OnInit } from '@angular/core';
import { FacultyService } from 'src/app/shared/services/faculty.service';

@Component({
  selector: 'app-list-faculty',
  templateUrl: './list-faculty.component.html',
  styleUrls: ['./list-faculty.component.scss']
})
export class ListFacultyComponent implements OnInit {
  faculties: any
  constructor(private facultyService:FacultyService) { }

  ngOnInit(): void {
    const ts = this.facultyService.getFacuties()
    this.faculties = ts
  }

}
