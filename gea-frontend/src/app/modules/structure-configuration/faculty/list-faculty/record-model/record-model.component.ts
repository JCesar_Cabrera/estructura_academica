import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, ReactiveFormsModule } from '@angular/forms';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { FacultyService } from 'src/app/shared/services/faculty.service';


@Component({
  selector: 'app-record-model',
  templateUrl: './record-model.component.html',
  styleUrls: ['./record-model.component.scss']
})
export class RecordModelComponent implements OnInit {

  facultyForm = new FormGroup({
    code: new FormControl(null),
    nameFaculty: new FormControl(null),
    deanName: new FormControl(null),
    address: new FormControl(null),
    telephone: new FormControl(null),
    email: new FormControl(null),
    mission: new FormControl(null),
    vision: new FormControl(null),
  })
  
  modal:NgbModalRef

  constructor(private modalService: NgbModal, private facultyService: FacultyService) { }

  ngOnInit(): void {
  }
  openLg(content : any){
    this.modal = this.modalService.open(content, { size: 'lg' });
  }
  closeLg(){
    this.facultyForm.reset()
    this.modal.close()
  }
  saveFaculty(){
    console.log(this.facultyForm.getRawValue());
    this.facultyService.saveFacuty(this.facultyForm.getRawValue());
    this.facultyForm.reset()
    this.modal.close()
  }
}
