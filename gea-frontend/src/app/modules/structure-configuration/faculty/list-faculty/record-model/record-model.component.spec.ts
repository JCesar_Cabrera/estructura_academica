import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecordModelComponent } from './record-model.component';

describe('RecordModelComponent', () => {
  let component: RecordModelComponent;
  let fixture: ComponentFixture<RecordModelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecordModelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecordModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
