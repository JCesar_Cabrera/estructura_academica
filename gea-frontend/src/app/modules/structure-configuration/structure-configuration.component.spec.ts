import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StructureConfigurationComponent } from './structure-configuration.component';

describe('StructureConfigurationComponent', () => {
  let component:StructureConfigurationComponent;
  let fixture: ComponentFixture<StructureConfigurationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StructureConfigurationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StructureConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
