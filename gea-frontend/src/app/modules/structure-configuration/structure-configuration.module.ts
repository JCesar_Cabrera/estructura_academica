import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InlineSVGModule} from 'ng-inline-svg';
import { StructureConfigurationComponent } from './structure-configuration.component';
import { StructureConfigurationRoutingModule } from './structure-configuration-routing.module';
import { FacultyComponent } from './faculty/faculty.component';
import { ListFacultyComponent } from './faculty/list-faculty/list-faculty.component';
import { DetailModelComponent } from './faculty/list-faculty/detail-model/detail-model.component';
import { RecordModelComponent } from './faculty/list-faculty/record-model/record-model.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
    declarations: [
      StructureConfigurationComponent,
      FacultyComponent,
      ListFacultyComponent,
      DetailModelComponent,
      RecordModelComponent
    ],
    imports: [
      CommonModule,
      StructureConfigurationRoutingModule,
      InlineSVGModule,
      ReactiveFormsModule
    ]
  })
  export class StructureConfigurationModule { }