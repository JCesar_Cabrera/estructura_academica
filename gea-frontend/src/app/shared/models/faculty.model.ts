export interface Faculty {
    deanName: string;
    address: string;
    mission: string;
    vision: string;
    telephone: string;
    email: string;
}