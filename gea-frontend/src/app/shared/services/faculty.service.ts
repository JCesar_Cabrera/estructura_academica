import { Injectable } from "@angular/core"

@Injectable({providedIn:'root'})

export class FacultyService {
    private mock = [
        {
            code: 15320,
            nameFaculty: 'Humanidades',
            deanName: 'Cristian',
        }
    ]
    getFacuties():any{
        return this.mock
    }
    saveFacuty(datos:any){
        return this.mock.push(datos)
    }
}